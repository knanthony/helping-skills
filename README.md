# Helping Skills

This site was developed to facilitate training in basic counseling skills with a particular focus on helping students transition from reading and learning about helping skills to face to face practice in laboratory settings.

This site provides students with the opportunity to record how they might respond to client statements and then watch how an experienced helper might respond in the same situation.

Instructors also have the opportunity to record feedback for each student based on the responses they give to client statements.